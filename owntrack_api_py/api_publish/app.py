import json

# import requests


def lambda_handler(event, context):
    """Sample pure Lambda function

    Parameters
    ----------
    event: dict, required
        API Gateway Lambda Proxy Input Format

        Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format

    context: object, required
        Lambda Context runtime methods and attributes

        Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Returns
    ------
    API Gateway Lambda Proxy Output Format: dict

        Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
    """
    isEventValid = False
    owntrackEvent = ""
    print(event)
    
    try: 
        print ("Event Body: " + event["body"])
        print ("Event User: " + event["headers"]["X-Limit-U"])
        print ("Event Device: " + event["headers"]["X-Limit-D"])
        print ("Event User Agent: " + event["headers"]["User-Agent"])
        
        owntrackEvent = json.loads(event["body"])   # Convert to Python Object
        
        #print ((owntrackEvent))
        print ("Event Type: " + str(owntrackEvent["_type"]))
        print ("Location Altitude: " + str(owntrackEvent["alt"]))
        print ("Location Lattitude: " + str(owntrackEvent["lat"]))
        print ("Location Longitude: " + str(owntrackEvent["lon"]))
        print ("Location Timestamp: " + str(owntrackEvent["tst"]))
        
        isEventValid = True
    except:
        print ("Failed to Parse Event. Logging Full Event: " + str(event))    
        

    # try:
    #     ip = requests.get("http://checkip.amazonaws.com/")
    # except requests.RequestException as e:
    #     # Send some context about this error to Lambda Logs
    #     print(e)

    #     raise e

    return {
        "statusCode": 200,
        "body": json.dumps({
            # "message": "owntracks publish recieved",
            # "location": ip.text.replace("\n", "")
        }),
    }
