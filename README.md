# Serverless Owntracks Service

The aim of this project is to create a thin backend system implemented with a serverless architecture for our owntracks applications. The aim is to design and build a serverless backend to persist and process incoming messages from http and mqtt traffic.

The API gateway calls a lambda function which stores the data to a dynamoDB.
This data is then pushed to queue, where it is then pushed to a frontend like thingsboard.


Create a static web page which invokes a AIM user to get credentials to read the last 10 location updates persisted to the system

The intended use of such a system is to create a persisted eventlog of messages to view at a later time. The system will consist of three components, namely:

1. AWS API Gateway
2. AWS Lambda function
3. Amazon DynamoDB which offers 25GB free within the Free Tier Offering.
4. Static Web Page

This project and processes must be documented in such a way to show competency in software backend  design.
All notes must be made available on gitlab source repository


## System Design
 
![alt text][SystemDesign]

[SystemDesign]: /docs/img/serverless_owntracks_design.png "System Design"


## Goals

- The initial milestone is to implement a http API gateway and persist incoming messages to a database.
- Translate the messages into a format required by thingsboard frontend and send via the thingsboard mqtt broker.
- View the GPS co-ordinates of a device in thingsboard, with a table of last few GPS logs and map.
- Because the data is stored in a database, it can then use to draw trip logs on a map or in a table. These visual indicators can also be done using serverless fontend tools, like Cloudfront and static pages. However this is for future scope.
- A valid use case for this work is to create a protocol adaptor for the Adeunis PULSE meter. They already provide Javascript Code for parsing their device



## Specifications

The owntracks message format is defined here: https://owntracks.org/booklet/guide/clients/.


## AWS services

The database must be implemented using dynamoDB on the AWS. This allows for using their free tier services.
The http interface must use the  API gateway along with a lambda function to receive and  persist the messages to the database and then send the message to an AWS SQS queue for further processing by a second lambda function.  
This second lambda function must parse and relay the processed massage to a topic via a mqtt broker in a standardised system JSON format.


## Milestones

#### Receiving Owntracks messages into lambda function
Receiving owntracks messages from documented API gateway and into Lambda function.

#### Message Persistence
The ability to persist the messages to an eventlog table in a database, for event replay purposes.

#### Message Translation
The ability to parse and translate an owntracks message format into a custom defined json object that follows a common message format to an internal system.

#### Visualizing Location Updates
To view GPS positions in a Thingboards Dashboard. This is outside the scope of this project, but will serve as a good way to view data for future projects


# Design Points

## Database
- Using DynamoDB
- PrimaryKey: User
- SortKey: DateTime
- 


## Local DynamoDB testing

### List tables
aws dynamodb list-tables

### Create Tablet

aws dynamodb create-table \
--table-name owntracksdb \
--attribute-definitions \
  AttributeName=user,AttributeType=S AttributeName=Date,AttributeType=S \
--key-schema \
  AttributeName=user,KeyType=HASH AttributeName=Date,KeyType=RANGE \
--provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5


aws dynamodb describe-table --table-name owntracksdb